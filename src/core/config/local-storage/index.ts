import type { Player } from '../../../data';
import { captureExeptionLog } from '../../log';

function storageOperation() {
    const setStorage = (key: string, val: Player[]) => {
        try {
            localStorage.setItem(key, JSON.stringify(val));
            return true;
        } catch (error) {
            captureExeptionLog(error);
        }
    };

    const getStorage = (key: string) => {
        try {
            const value = localStorage.getItem(key);
            if (value !== null) {
                return JSON.parse(value);
            } else {
                return null;
            }
        } catch (error) {
            captureExeptionLog(error);
        }
    };

    const removeStorage = (key: string) => {
        try {
            localStorage.removeItem(key);
            return true;
        } catch (error) {
            captureExeptionLog(error);
        }
    };

    return {
        setStorage,
        getStorage,
        removeStorage,
    };
}
export default storageOperation;
