export function setGameLevel(level: string) {
    switch (level) {
        case 'easy':
            return 25;
        case 'medium':
            return 10;
        case 'hard':
            return 5;
        default:
            return 5;
    }
}
