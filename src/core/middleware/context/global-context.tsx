import { createContext, useState } from 'react';
import { defautPlayerState, Player } from '../../../data';

export interface GlobalContextInterface {
    player: Player;
    leaderboards: Player[] | null;
    updatePlayerName: (val: string) => void;
    updatePlayerLevel: (val: string) => void;
    updatePlayerScore: (val: number) => void;
    updateLeaderBoard: (val: any[]) => void;
}

const defaultState = {
    player: defautPlayerState,
    leaderboards: [],
};

export const GlobalContext = createContext<Partial<GlobalContextInterface>>(defaultState);

export const GlobalProvider = (props: any) => {
    const [player, setPlayer] = useState<Player>(defautPlayerState);
    const [leaderboards, setLeaderBoards] = useState<Player[]>([]);

    const updatePlayerName = (val: string) => {
        setPlayer((prevState) => ({
            ...prevState,
            name: val,
        }));
    };

    const updatePlayerLevel = (val: string) => {
        setPlayer((prevState) => ({
            ...prevState,
            level: val,
        }));
    };

    const updatePlayerScore = (val: number) => {
        setPlayer((prevState) => ({
            ...prevState,
            score: val,
        }));
    };

    const updateLeaderBoard = (val: Player[]) => {
        setLeaderBoards(val);
    };

    return (
        <GlobalContext.Provider
            value={{
                player: player,
                leaderboards: leaderboards,
                updatePlayerScore: updatePlayerScore,
                updatePlayerName: updatePlayerName,
                updatePlayerLevel: updatePlayerLevel,
                updateLeaderBoard: updateLeaderBoard,
            }}
        >
            {props.children}
        </GlobalContext.Provider>
    );
};
