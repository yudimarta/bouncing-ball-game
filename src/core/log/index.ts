import * as Sentry from '@sentry/react';

export function captureExeptionLog(error: any) {
    Sentry.captureException(error);
}
