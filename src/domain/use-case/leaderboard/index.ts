import getLeaderBoardUseCase from './get-leaderboard';
import setLeaderBoardUseCase from './set-leaderboard';

export { getLeaderBoardUseCase, setLeaderBoardUseCase };
