import { useContext } from 'react';
import { LeaderboardRepositories } from '../../../data';
import { captureExeptionLog, GlobalContext } from '../../../core';

export default function setLeaderBoardUseCase(data: any) {
    const { setLeaderBoardStorage } = LeaderboardRepositories();
    try {
        setLeaderBoardStorage(data);
    } catch (error) {
        captureExeptionLog(error);
    }
}
