import { LeaderboardRepositories } from '../../../data';
import { captureExeptionLog } from '../../../core';

export default function getLeaderBoardUseCase() {
    const { getLeaderBoardStorage } = LeaderboardRepositories();
    try {
        const list = getLeaderBoardStorage();
        if (list !== null) {
            return list;
        }
    } catch (error) {
        captureExeptionLog(error);
    }
}
