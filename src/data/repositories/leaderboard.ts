import { captureExeptionLog, StorageKey } from '../../core';
import storageOperation from '../../core/config/local-storage';
import { Player } from '../models';

const { getStorage, setStorage } = storageOperation();

const LeaderboardRepositories = () => {
    function getLeaderBoardStorage() {
        try {
            const leaderboard = getStorage(StorageKey.leaderBoards);
            if (leaderboard !== null) {
                return leaderboard;
            }
        } catch (error) {
            captureExeptionLog(error);
        }
    }

    function setLeaderBoardStorage(value: Player[]) {
        try {
            setStorage(StorageKey.leaderBoards, value);
        } catch (error) {
            captureExeptionLog(error);
        }
    }

    return {
        getLeaderBoardStorage,
        setLeaderBoardStorage,
    };
};

export default LeaderboardRepositories;
