export interface Player {
    name: string;
    level: string;
    score: number;
}

export const defautPlayerState: Player = {
    name: '',
    level: 'hard',
    score: 0,
};
