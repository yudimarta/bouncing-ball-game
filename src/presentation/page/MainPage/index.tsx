import { useContext } from 'react';
import { GlobalContext, SCORES, setGameLevel } from '../../../core';
import { Ball, Button, GameOverModal, MainMenuModal, Score, ScoreSection } from '../../components';
import useViewModel from './MainPageViewModel';

function MainPage() {
    const { player } = useContext(GlobalContext);
    const {
        buttonText,
        isDisabled,
        showGameOverModal,
        setShowGameoverModal,
        showMainMenu,
        setShowMainMenu,
        reset,
        jumpBall,
        resetPlayer,
    } = useViewModel();

    return (
        <div className="main-container">
            <Score score={player!.score} />
            {SCORES.map((items: number) => {
                return <ScoreSection score={items} key={items} />;
            })}
            <div className="control-section-container">
                <div className="control-button-container">
                    <Button
                        disabled={isDisabled}
                        id="jump-btn"
                        text={buttonText}
                        onClick={() => {
                            jumpBall(player!.score, setGameLevel(player!.level));
                        }}
                    />
                </div>
            </div>
            <Ball />
            {showGameOverModal && (
                <GameOverModal
                    score={player!.score}
                    onBackMenu={() => {
                        setShowGameoverModal(false);
                        setShowMainMenu(true);
                        reset();
                        resetPlayer();
                    }}
                    onRetry={() => {
                        setShowGameoverModal(false);
                        reset();
                    }}
                />
            )}
            {showMainMenu && (
                <MainMenuModal
                    onClickPlay={() => {
                        setShowMainMenu(false);
                    }}
                />
            )}
        </div>
    );
}

export default MainPage;
