import { useCallback, useContext, useEffect, useState } from 'react';
import { captureExeptionLog, GlobalContext } from '../../../core';
import { Player } from '../../../data';
import { setLeaderBoardUseCase } from '../../../domain';

function MainPageViewModel() {
    const { updatePlayerScore, updatePlayerName, player, leaderboards, updateLeaderBoard } = useContext(GlobalContext);
    const [buttonText, setButtonText] = useState<'Start' | 'Jump' | 'Wait ...'>('Start');
    const [isDisabled, setIsDisabled] = useState(false);
    const [showGameOverModal, setShowGameoverModal] = useState(false);
    const [showMainMenu, setShowMainMenu] = useState(true);

    let timer: any;
    let marginTop = 0;
    let bottomToTop = true;

    useEffect(() => {
        return () => {
            clearInterval(timer);
        };
    }, []);

    // method to bounce the ball
    const bounceBall = useCallback(() => {
        const ball = document.getElementById('ball');
        if (bottomToTop) {
            if (marginTop > 0) {
                marginTop -= 1;
                setIsDisabled(true);
            }
        } else {
            if (marginTop < 75) {
                marginTop += 1;
                setIsDisabled(false);
                setButtonText('Jump');
            } else {
                ball!.style.display = 'none';
                setShowGameoverModal(true);
                marginTop = 0;
                clearInterval(timer);
            }
        }

        ball!.style.marginTop = `${marginTop}dvh`;
        if (marginTop === 0) {
            bottomToTop = false;
        }
    }, [isDisabled]);

    const setLeaderBoards = (playerData: Player[]) => {
        try {
            setLeaderBoardUseCase(playerData);
        } catch (error) {
            captureExeptionLog(error);
        }
    };

    // method to handle start/jump the ball
    const jumpBall = useCallback((latestScore: number, level: number) => {
        const btn = document.getElementById('jump-btn');
        if (btn!.innerHTML.trim() === 'Start') {
            clearInterval(timer);
            timer = setInterval(function () {
                bounceBall();
            }, level);
            setButtonText('Jump');
        } else {
            bottomToTop = true;
            if (!isDisabled) {
                if (marginTop < 25) {
                    updatePlayerScore!(latestScore + 1);
                } else if (marginTop < 45) {
                    updatePlayerScore!(latestScore + 2);
                } else if (marginTop < 60) {
                    updatePlayerScore!(latestScore + 3);
                } else if (marginTop < 70) {
                    updatePlayerScore!(latestScore + 4);
                } else if (marginTop < 75) {
                    updatePlayerScore!(latestScore + 5);
                }
            }
            setButtonText('Wait ...');
        }
    }, []);

    function reset() {
        setToInitialPosition();
        insertNewScore();
        setLeaderBoards([...leaderboards!, player!]);
    }

    function resetPlayer() {
        updatePlayerName!('');
        updatePlayerScore!(0);
    }

    function insertNewScore() {
        if (leaderboards && leaderboards.length > 0) {
            updateLeaderBoard!([...leaderboards, player]);
        } else {
            updateLeaderBoard!([player]);
        }
    }

    function setToInitialPosition() {
        bottomToTop = true;
        const ball = document.getElementById('ball');
        updatePlayerScore!(0);
        setButtonText('Start');
        ball!.style.display = 'block';
        ball!.style.marginTop = `${marginTop}dvh`;
        setIsDisabled(false);
    }

    return {
        buttonText,
        setButtonText,
        isDisabled,
        setIsDisabled,
        showGameOverModal,
        setShowGameoverModal,
        showMainMenu,
        setShowMainMenu,
        marginTop,
        bottomToTop,
        reset,
        jumpBall,
        bounceBall,
        resetPlayer,
    };
}

export default MainPageViewModel;
