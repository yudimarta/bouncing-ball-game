import React from 'react';

import './leaderboard-card.scss';
import classNames from 'classnames';
import { ProfilePlaceHolder } from '../../../assets';

interface LeaderBoardCardProps {
    data: any;
    index: number;
}

function LeaderBoardCard(props: LeaderBoardCardProps) {
    const { data, index } = props;
    const containerStyle = classNames({
        'leaderboard-container': true,
        'bg-gradient-05': index === 0,
        'bg-gradient-04': index === 1,
        'bg-gradient-03': index === 2,
    });
    return (
        <div className={containerStyle}>
            <div className="leaderboard-profile-container">
                <div className="profile-img-container">
                    <img src={ProfilePlaceHolder} alt="profile" />
                </div>
                <div className="w-[16px]"></div>
                <div className="leaderboard-name-container">
                    <p className="body01-bold">{data.name}</p>
                    <div className="h-[4px]"></div>
                    <p className="body02-regular">{data.level}</p>
                </div>
            </div>
            <h4>{data.score}</h4>
        </div>
    );
}

export default LeaderBoardCard;
