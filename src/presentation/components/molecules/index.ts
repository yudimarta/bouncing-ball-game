import GameOverModal from './game-over-modal';
import MainMenuModal from './main-menu-modal';
import Tabs from './tabs';
import TextField from './text-field';
import LeaderBoardCard from './leaderboard-card';

export { GameOverModal, MainMenuModal, Tabs, TextField, LeaderBoardCard };
