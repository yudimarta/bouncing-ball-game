import { GameOverIll } from '../../../assets';
import { Button } from '../../atoms';
import './game-over-modal.scss';

interface GameOverModalProps {
    score: number;
    onBackMenu: () => void;
    onRetry: () => void;
}

function GameOverModal(props: GameOverModalProps) {
    const { score, onBackMenu, onRetry } = props;
    return (
        <>
            <div className="gameover-modal-overlay"></div>
            <div className="modal-container">
                <div className="illustration-container">
                    <img src={GameOverIll} alt="Game Over" />
                </div>
                <h4 className="text-primary">Game Over</h4>
                <p className="text-secondary body01-regular">Your Score: {score}</p>
                <div className="button-container">
                    <Button text="Main Menu" type="secondary" onClick={onBackMenu} />
                    <div className="w-16"></div>
                    <Button text="Retry" onClick={onRetry} />
                </div>
            </div>
        </>
    );
}

export default GameOverModal;
