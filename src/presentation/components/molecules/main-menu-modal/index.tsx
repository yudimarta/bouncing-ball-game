import { useContext, useEffect, useState } from 'react';
import { captureExeptionLog, checkUsernameLength, GlobalContext, StorageKey } from '../../../../core';
import { getLeaderBoardUseCase } from '../../../../domain';
import { GameConsoleIll } from '../../../assets';
import { Button, Label, Tab } from '../../atoms';
import LeaderBoardCard from '../leaderboard-card';
import Tabs from '../tabs';
import TextField from '../text-field';
import './main-menu-modal.scss';

interface MainMenuModalProps {
    onClickPlay: () => void;
}

function MainMenuModal(props: MainMenuModalProps) {
    const { leaderboards, updatePlayerName, updatePlayerLevel, player, updateLeaderBoard } = useContext(GlobalContext);
    const { onClickPlay } = props;
    const [activeMenu, setActiveMenu] = useState<'leaderboard' | 'play'>('play');

    const getLeaderboard = () => {
        try {
            const list = getLeaderBoardUseCase();
            if (list) {
                updateLeaderBoard!(list);
                return list;
            }
        } catch (error) {
            captureExeptionLog(error);
        }
    };

    useEffect(() => {
        getLeaderboard();
    }, []);

    return (
        <>
            <div className="main-modal-overlay"></div>
            <div className="main-modal-container">
                <div className="main-illustration-container hover:animate-bounce">
                    <img src={GameConsoleIll} alt="Game Console" />
                    <div className="w-[16px]"></div>
                    <h4>Bounce Your Ball!</h4>
                </div>
                <Tabs>
                    <Tab
                        label="Play!"
                        selected={activeMenu === 'play'}
                        onClick={() => {
                            setActiveMenu('play');
                        }}
                    />
                    <Tab
                        label="Leader Board"
                        selected={activeMenu === 'leaderboard'}
                        onClick={() => {
                            setActiveMenu('leaderboard');
                        }}
                    />
                </Tabs>
                {activeMenu === 'play' ? (
                    <div className="play-section-container">
                        <p className="body01-regular text-primary">
                            Don&apos;t let the ball touch the bottom!! <br /> Get higher score when your ball closer to
                            the bottom.
                        </p>
                        <div className="h-[24px]"></div>
                        <TextField
                            onChange={(e: any) => {
                                // setUserName(e.target.value);
                                updatePlayerName!(e.target.value);
                            }}
                        />
                        <div className="h-[8px]"></div>
                        <Label label="Difficulty Level" isMandatory />
                        <div className="h-[8px]"></div>
                        <div className="difficulties-button-container">
                            <button
                                data-testid="easy_button"
                                className={`level-button ${player!.level === 'easy' ? 'selected' : ''}`}
                                onClick={() => {
                                    updatePlayerLevel!('easy');
                                }}
                            >
                                Easy
                            </button>
                            <div className="w-[8px]"></div>
                            <button
                                data-testid="medium_button"
                                className={`level-button ${player!.level === 'medium' ? 'selected' : ''}`}
                                onClick={() => {
                                    updatePlayerLevel!('medium');
                                }}
                            >
                                Medium
                            </button>
                            <div className="w-[8px]"></div>
                            <button
                                data-testid="hard_button"
                                className={`level-button ${player!.level === 'hard' ? 'selected' : ''}`}
                                onClick={() => {
                                    updatePlayerLevel!('hard');
                                }}
                            >
                                Hard
                            </button>
                        </div>
                        <div className="h-[16px]"></div>
                        <Button text="Play" onClick={onClickPlay} disabled={checkUsernameLength(player!.name)} />
                    </div>
                ) : (
                    <div className="leaderboard-section-container">
                        <>
                            {leaderboards! && leaderboards.length > 0 ? (
                                leaderboards
                                    .sort((a, b) => b.score - a.score)
                                    .slice(0, 3)
                                    .map((item: any, index) => {
                                        return <LeaderBoardCard data={item} key={index} index={index} />;
                                    })
                            ) : (
                                <div className="empty-leaderboard-container">
                                    <p className="body02-bold text-primary">
                                        Oops, you can be on the top of leader board, <br /> let&apos;s play!
                                    </p>
                                </div>
                            )}
                        </>
                    </div>
                )}
                <p className="text-primary body02-regular">© 2023 Yudi Marta Arianto</p>
            </div>
        </>
    );
}

export default MainMenuModal;
