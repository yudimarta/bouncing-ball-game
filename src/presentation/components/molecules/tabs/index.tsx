import React from 'react';

interface TabsProps {
    children: React.ReactNode;
}

function Tabs(props: TabsProps) {
    const { children } = props;
    return <div className="flex relative w-full border-b-[1px] border-gray01">{children}</div>;
}

export default Tabs;
