import React from 'react';
import { Label } from '../../atoms';
import './text-field.scss';

interface TextFieldProps {
    onChange: (val: any) => void;
}

function TextField(props: TextFieldProps) {
    const { onChange } = props;
    return (
        <div className="input_container">
            <Label label="Username" isMandatory />
            <div className="body02-regular">
                <input
                    data-testid="username_input"
                    className="input_style"
                    placeholder="Input your username"
                    maxLength={20}
                    onChange={onChange}
                />
            </div>
            <div className="body02-regular mt-[4px]">
                <p className="supporting_text">* minimum 3 characters</p>
            </div>
        </div>
    );
}

export default TextField;
