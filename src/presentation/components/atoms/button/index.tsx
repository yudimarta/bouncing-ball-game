import './button.scss';
import classNames from 'classnames';

interface ButtonProps {
    text: string;
    id?: string;
    type?: string;
    disabled?: boolean;
    onClick: () => void;
}

function Button(props: ButtonProps) {
    const { text, type, onClick, disabled, id } = props;
    const btnStyle = classNames({
        'common-btn': true,
        secondary: type === 'secondary',
        text: type === 'text',
    });
    return (
        <button id={id} disabled={disabled} className={btnStyle} onClick={onClick}>
            {text}
        </button>
    );
}

export default Button;
