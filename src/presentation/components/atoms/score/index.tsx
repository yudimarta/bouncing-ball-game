import React from 'react';
import './score.scss';

interface ScoreProps {
    score: number;
}

function Score(props: ScoreProps) {
    const { score } = props;
    return (
        <div className="score-container">
            <p className="text-white title-bold">score</p>
            <h1 id="score" data-testid="score">
                {score}
            </h1>
        </div>
    );
}

export default Score;
