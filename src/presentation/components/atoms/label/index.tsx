import React from 'react';

interface LabelProps {
    label: string;
    isMandatory: boolean;
}

function Label(props: LabelProps) {
    const { label, isMandatory } = props;
    return (
        <div className="flex flex-row items-center">
            <p className="body02-regular text-primary">
                {label} {isMandatory && <span className="text-red">*</span>}{' '}
            </p>
        </div>
    );
}

export default Label;
