import React from 'react';
import './tab.scss';

interface TabProps {
    label: string;
    onClick: () => void;
    selected: boolean;
}

function Tab(props: TabProps) {
    const { label, onClick, selected } = props;
    return (
        <button className="tab-container" onClick={onClick}>
            <div className="flex flex-row items-center justify-center">
                <p className="body01-bold">{label}</p>
            </div>
            {selected && <div className="indicator"></div>}
        </button>
    );
}

export default Tab;
