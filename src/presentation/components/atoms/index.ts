import Button from './button';
import Ball from './ball';
import ScoreSection from './score-section';
import Score from './score';
import Tab from './tab';
import Label from './label';

export { Button, Ball, ScoreSection, Score, Tab, Label };
