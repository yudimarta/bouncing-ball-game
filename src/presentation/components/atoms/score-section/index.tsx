import classNames from 'classnames';
import './score-section.scss';

interface ScoreSectionProps {
    score: number;
}

function ScoreSection(props: ScoreSectionProps) {
    const { score } = props;
    const scoreSectionStyle = classNames({
        'score-section': true,
        'section-1': score === 1,
        'section-2': score === 2,
        'section-3': score === 3,
        'section-4': score === 4,
        'section-5': score === 5,
    });
    return <div className={scoreSectionStyle}>{score}</div>;
}

export default ScoreSection;
