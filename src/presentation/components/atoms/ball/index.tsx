import React from 'react';
import './ball.scss';

function Ball() {
    return <div id="ball" className="circle"></div>;
}

export default Ball;
