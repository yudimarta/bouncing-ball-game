import renderer from 'react-test-renderer';
import { MainPage } from '../../presentation';
import {
    Ball,
    Button,
    GameOverModal,
    Label,
    LeaderBoardCard,
    MainMenuModal,
    Score,
    ScoreSection,
    Tab,
    Tabs,
    TextField,
} from '../../presentation/components';

describe('renders Component correctly', () => {
    test('renders Ball Component correctly', () => {
        const ball = renderer.create(<Ball />).toJSON();
        expect(ball).toMatchSnapshot();
    });

    test('renders Button Component correctly', () => {
        const button = renderer.create(<Button text="Button" onClick={() => console.log('clicked')} />).toJSON();
        expect(button).toMatchSnapshot();
    });

    test('renders Label Component correctly', () => {
        const label = renderer.create(<Label label="name" isMandatory />).toJSON();
        expect(label).toMatchSnapshot();
    });

    test('renders Score Component correctly', () => {
        const score = renderer.create(<Score score={20} />).toJSON();
        expect(score).toMatchSnapshot();
    });

    test('renders Score Section Component correctly', () => {
        const scoreSection = renderer.create(<ScoreSection score={1} />).toJSON();
        expect(scoreSection).toMatchSnapshot();
    });

    test('renders Tab Component correctly', () => {
        const tab = renderer.create(<Tab label="Play" onClick={() => console.log('clicked')} selected />).toJSON();
        expect(tab).toMatchSnapshot();
    });

    test('renders Game Over Modal correctly', () => {
        const gameOverModal = renderer
            .create(
                <GameOverModal
                    score={100}
                    onBackMenu={() => console.log('clicked')}
                    onRetry={() => console.log('clicked')}
                />
            )
            .toJSON();
        expect(gameOverModal).toMatchSnapshot();
    });

    test('renders Leader Board Card correctly', () => {
        const dummyData = {
            name: 'Haaland',
            level: 'hard',
            score: 100,
        };
        const leaderboardCard = renderer.create(<LeaderBoardCard data={dummyData} index={1} />).toJSON();
        expect(leaderboardCard).toMatchSnapshot();
    });

    test('renders Main Menu Modal correctly', () => {
        const mainMenuModal = renderer.create(<MainMenuModal onClickPlay={() => console.log('clicked')} />).toJSON();
        expect(mainMenuModal).toMatchSnapshot();
    });

    test('renders Tabs correctly', () => {
        const tabs = renderer
            .create(
                <Tabs>
                    <Tab label="Play" onClick={() => console.log('clicked')} selected />
                </Tabs>
            )
            .toJSON();
        expect(tabs).toMatchSnapshot();
    });

    test('renders Textfield Component correctly', () => {
        const textField = renderer.create(<TextField onChange={(e: any) => console.log(e)} />).toJSON();
        expect(textField).toMatchSnapshot();
    });

    test('renders Main Page correctly', () => {
        const mainPage = renderer.create(<MainPage />).toJSON();
        expect(mainPage).toMatchSnapshot();
    });
});
