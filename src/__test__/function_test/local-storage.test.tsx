import { StorageKey } from '../../core';
import storageOperation from '../../core/config/local-storage';

describe('Local Storage Test', () => {
    const { setStorage, getStorage, removeStorage } = storageOperation();
    const dummyItem = [
        {
            name: 'Haaland',
            level: 'hard',
            score: 100,
        },
    ];
    it('store Data to Local Storage', () => {
        expect(setStorage(StorageKey.leaderBoards, dummyItem)).toBeDefined();
    });

    it('get Data from Local Storage', () => {
        expect(getStorage(StorageKey.leaderBoards)).toBeDefined();
    });

    it('remove Data from Local Storage', () => {
        expect(removeStorage(StorageKey.leaderBoards)).toBeDefined();
    });
});
