import { SCORES } from '../../core';

test('Check Scoring range to be 5', () => {
    expect(SCORES.length).toBe(5);
});
