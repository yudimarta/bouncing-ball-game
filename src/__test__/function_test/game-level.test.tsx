import { setGameLevel } from '../../core';

test('Interval Range based on level string input', () => {
    expect(setGameLevel('easy')).toBe(25);
    expect(setGameLevel('medium')).toBe(10);
    expect(setGameLevel('hard')).toBe(5);
    expect(setGameLevel('undefined')).toBe(5);
});
