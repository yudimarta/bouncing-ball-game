import { checkUsernameLength } from '../../core';

test('check username length to be minimum 3 characters', () => {
    expect(checkUsernameLength('Azka')).toBe(false);
    expect(checkUsernameLength('Az')).toBe(true);
});
