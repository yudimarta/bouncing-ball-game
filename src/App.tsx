import './App.scss';
import './presentation/styles/theme.scss';
import { MainPage } from './presentation';
import * as Sentry from '@sentry/react';
import { GlobalProvider } from './core';

function App() {
    return (
        <GlobalProvider>
            <div className="App">
                <MainPage />
            </div>
        </GlobalProvider>
    );
}

export default Sentry.withProfiler(App);
