# Bounce Your Ball

Bounce Your Ball is a web based game with the objective to avoid the ball reach the bottom with clicking Jump Button. The more closer to the bottom when click the button, player will get higher score.

## Assumptions:

1. Bounce your Ball is a web application
2. Leaderboard data stored on local storage (Offline)

## Dependencies:

1. Build using React v18.2.0
2. Using SCSS and Tailwindcss as CSS Framework
3. Using Firebase Hosting as Hosting Service
4. Using Sentry as Error Tracker and Performance Monitoring
5. Using third party libraries:
    - [@sentry/react](https://sentry.io/)
    - [classnames](https://www.npmjs.com/package/classnames)
    - [firebase](https://firebase.google.com/)
    - [eslint](https://eslint.org/)
    - [sass](https://sass-lang.com/)

**App Architecture**

```mermaid
graph TD
    A[Main Page] -->B[Main Page ViewModel]
    A -->E
    B -->C[Leaderboard Use Case]
    C -->D[Leaderboard Data Source]
    D -->E[Leaderboard Adapter]
    B -->E
    C -->E
    D -->E
    A -->F[Player Adapter]
    B -->F
```

## Instruction

### Running Development Server

Clone this repository and run to install all dependencies

```bash
npm install
```

Create .env file which contain base url for the API at root of project **(delete soon)**

```javascript
REACT_APP_SENTRY_DSN=https://6feb95caeb8840b39d73a30456bb9500@o911883.ingest.sentry.io/4505298662260736
REACT_APP_WEB_URL=https://bounce-your-ball-tsel.web.app/
REACT_APP_FIREBASE_APIKEY=AIzaSyCfCvetga_8xKU42rte0-rOSRnsYKw3cq0
REACT_APP_FIREBASE_AUTH_DOMAIN=bounce-your-ball-tsel.firebaseapp.com
REACT_APP_FIREBASE_PROJECT_ID=bounce-your-ball-tsel
REACT_APP_FIREBASE_STORAGE_BUCKET=bounce-your-ball-tsel.appspot.com
REACT_APP_FIREBASE_MESSAGING_SENDER_ID=935206212144
REACT_APP_FIREBASE_APP_ID=1:935206212144:web:9bea0448595552781f53f5
REACT_APP_FIREBASE_MEASUREMENT_ID=G-9TERPH3RGY
```

Run development server mode

```bash
npm start
```

open your browser and open `http://localhost:3000/`

### Generating Production Build

Make sure you have done the steps on the Development server running, and run to create production build

```bash
npm run build
```

check root folder of your project, you will find new `build` directory

### Perform Testing

```bash
npm run test
```

### Check Test Coverage

```bash
npm run coverage
```

![Screen Shot 2023-06-05 at 08.01.21.png](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2008.01.21.png?alt=media&token=65d24c8c-d91b-4f25-bb6d-9c2b33500217&_gl=1*1o8pii1*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkyNjg5OS43LjEuMTY4NTkyNjk1Mi4wLjAuMA..)

### Performance Test

Using Chrome Dev Tools Performance Test to check memory heap consumption and garbage collection.

![Screen_Shot_2023-06-05_at_04.48.45](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2004.48.45.png?alt=media&token=3531ada5-033a-4b07-858e-90a9ddd2e7cc&_gl=1*cw7zzi*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTMwNS4wLjAuMA..)

on the graph trend we can assume the garbage collection works effectively to avoid memory leaks

### Web Vitals Test Result

![Screen_Shot_2023-06-05_at_04.45.04](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2004.45.04.png?alt=media&token=c3cfe93e-aaa4-4efb-94a1-aa7514afe8b6&_gl=1*mqnddi*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTM3NC4wLjAuMA..)

## Monitoring

### Sentry Issue and Performance Tracker

![Screen_Shot_2023-06-05_at_04.49.19](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2004.49.19.png?alt=media&token=0ba0aa81-7f9d-41a1-a0bd-c4db30ca0915&_gl=1*9xav70*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTQxNi4wLjAuMA..)

![Screen_Shot_2023-06-05_at_05.11.54](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2005.11.54.png?alt=media&token=820d28ad-30a9-47b5-8d67-9012fa33a85b&_gl=1*iqvg6m*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTQzNC4wLjAuMA..)

![Screen_Shot_2023-06-05_at_04.50.20](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2004.50.20.png?alt=media&token=3575441d-0864-4d45-bdc3-b3c74e81c5b2&_gl=1*760886*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTQ1NC4wLjAuMA..)

### Firebase Analytics

![Screen_Shot_2023-06-05_at_04.51.16](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2004.51.16.png?alt=media&token=ad4ef929-40d8-4983-ab97-5c8c485598ed&_gl=1*1gycraw*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTQ4Ny4wLjAuMA..)

![Screen_Shot_2023-06-05_at_04.52.39](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2004.52.39.png?alt=media&token=3d1aeb86-92e4-4129-aa69-ce33eea76200&_gl=1*dxa2q2*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTQ5OC4wLjAuMA..)

## User Guide

-   Open your browser, and go to `https://bounce-your-ball-tsel.web.app/`

![Screen_Shot_2023-06-05_at_05.19.58](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2005.19.58.png?alt=media&token=8de9a4a1-72af-4a96-ac09-d814289b7416&_gl=1*cyj2d4*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTUxNi4wLjAuMA..)

-   Fill in the username in the form that has been provided, and select difficulty level
-   Click Start button, the ball will move up and down. Don't let the ball touch the bottom by keep clicking the button.
-   The more closer to the bottom when click the button, player will get higher score.

![Screen_Shot_2023-06-05_at_05.36.44](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2005.36.44.png?alt=media&token=9a925803-e4ea-4825-bbbd-3d852732ff17&_gl=1*1c02gmc*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTU0Ni4wLjAuMA..)

-   If the ball touch the bottom, final score will calculated and pop up modal will appear

![Screen_Shot_2023-06-05_at_05.36.52](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2005.36.52.png?alt=media&token=293e4de5-e195-4e1f-bf37-7f41bafc364a&_gl=1*5lt49g*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTU2My4wLjAuMA..)

-   You can select Retry button to start over with same username and difficulty level
-   Or, you can back to main menu to continue with different username and/or difficulty level
-   You can also check the leaderboard (Only 3 best score shown)

![Screen_Shot_2023-06-05_at_05.36.58](https://firebasestorage.googleapis.com/v0/b/bounce-your-ball-tsel.appspot.com/o/Screen%20Shot%202023-06-05%20at%2005.36.58.png?alt=media&token=f540a7d9-f395-401d-b494-f9c84e1ae279&_gl=1*1uewuae*_ga*MTExNTMzNjY4OC4xNjg1ODQ0OTMy*_ga_CW55HF8NVT*MTY4NTkxODU0My42LjEuMTY4NTkxOTU3OC4wLjAuMA..)

## Future Enhancement

-   Using Firebase Authentication to store and manage user
-   Using Firebase Realtime Database to store user's score

## Website

[Bounce Your Ball!](https://bounce-your-ball-tsel.web.app/)
